# junior-dev-code-challenge
Coding challenge

HOW TO USE /src/challenge.py

	by default this command will use dictionary.txt if present in the same directory:
	python3 challenge.py [word] 

	This command will use any other file you give it:
	python3 challenge.py [word] [path_to_file]

	This command will retrieve the encrypted file, decrypt it using the key with the same name and find anagrams.
	python3 challenge.py [word] [https://link/to/file.txt.rsa]


HOW TO USE /src/rsa/rsa_encryption.py

	This command will encrypt any file you give it and create: [file.txt.rsa] which is the encrypted file and [file.txt.key] which is the Decryption key
	python3 rsa_encryption.py encrypt [file.txt] 

	This will decrypt any file and spit create [file.txt]
	python3 rsa_encryption decrypt [file.txt.rsa] [file.txt.key]


HOW TO USE THE ISO:

	Use the iso named anagram.iso, you have two options to launch this operating system.
	
	Its is located in the /iso file

	OPTION 1:
		Burn it to a usb (I recommend using Fedora media writter for that but any other will do)
		Boot it up -preferably on a legacy system preferably- and select the first option Load Anagram Finder To RAM.

	OPTION 2:
		Use Quemu or any Virtualization software and do a legacy boot preferably as well. If you chose to load it that way DO NO SELECT THE FIRST OPTION,
		SELECT THE SECOND OPTION Start Anagram Finder.

	Once the iso is fully loaded it will ask you for a Login, simply login as 'root' no password required.

	When you are logged in connect to the internet, if you need a graphical user interface to do that you can start one by typing the command 'startx'.

	To launch the anagram App type the command 'anagram' followed by the word you are trying to find an anagram for, E.g. anagram testing.

	enjoy.

BRIEF OVERVIEW:
	
	The iso image is of course volatile I created it with all the needed resources to run the application, the first time you launch the command checks to see if a
	Github is up, if it is not i assume that the computer is not connected to the internet.
	After that is uses the wget to retrieve dictionaty.txt.rsa, this file has every character encrypted using an RSA encryption. The decryption key is contained in  /usr/junior_dev_code_challenge/data/dictionary.txt.key on the iso. This file is decrypted and the command takes the first argument as the word to look for.

FILE DESCRIPTION:

	/Data This folder contains the data  that will be used(for now only one file)
		/Data/dictionary.txt 
		This Text document is dictionary.txt encrypted using RSA

	/src This contains folder contains all the Python scripts
		/src/dictionary.txt
		/src/challenge.py
		This Python script is written for python3 and is my solution to the challenge 
		/src/test_challenge.py
		This Python script is my test unit for the function in challenge.py

		/rsa This Folder only contains the Python scripts related to the RSA encryption 
			/rsa/miller_rabin.py I decide to keep the function in this file separate as it is not mine, i solves the Miller Rabin alghritm that I was not familiar with
			/rsa/rsa_encryption.py this Python script contains all the functions used to Encrypt and Decrypt files
			/rsa/test_rsa_encryption.py this python script is contains unit tests for the functions in rsa_encryption.py

	/.instruction This folder contains the instructions you send me

	/iso/ This folder contains all the files used to create the anagram.iso
		/iso/fluent-city.jpg This is the background image that appears when booting up in legacy mode
		/isolinux.cfg This configuration file is used to configure the boot options of the iso
		/command.sh This bash script contains the command used when typing 'anagram [word]'
		/personal-spin This folder contains all the kickstater scripts used to creat the fedora iso, these files are part of the open source kickstarter-spin
			/personal-spin/live-base.ks This kickstarter script is the main script used to create the iso 


I would like to answer those questions separately as I feel that they are important and comments will not be enough to express myself on those points.

Explanations behind why you chose the approach you did
	
	I chose this approach because i always wanted to implement the RSA encryption system using python, the version i made is very very simple as i did not have the time to optimize to use larger number, the random number generator can create very large numbers for more cryptographic use but my version of RSA would -with a computer of moderate power- take too long to encrypt and decrypt large blocks of information for me to be able to implement is.

	Creating the iso was a way for me to simplify the usage of my application and demonstrate my knowledge of linux, i also like to have my own environment running on different computers as it gives me control over the environment my applications will run in.

	I initially wanted to host the file on Heroku will and create a Django app to share the file, but i was short on time so i simply decided to keep ip on github,
	Hosting my software of pieces of it remotely allows me to modify the source code without having to update the iso to run it properly.
	Some of the software i use is only hosted on my companies share so it cannot be accessed outside the company and the usb s i run them on send me alert if anyone attempts to run the commands outside the company. 


Problem areas or future concerns (e.g. how might the code scale in production)
	
	The encryption keys are too small so it is not suitable for sensitive data. for speed i also encrypted files character by character so it makes it resemble a Cesar Cypher which is very weak. 

	If the file gets too large downloading and decrypting might take too long.

	The good news is that I made sure to load all data into memory in chunks to avoid memory issues even as the file gets larger.

	The Operating system loads into memory but the space it occupies is constant so i would be negligible on more powerful systems.

	Not using storage makes the system run faster as all files are created on and accessed in memory.


Why you wrote the unit tests you did and what they guard against
	
	My unit test guard against arithmetic errors for the rsa_encryption.py as it is the most problematic area in cryptography, and logic errors in challenge.py as it is easy to mess up the logic when solving problems of that kind


Alternative implementations/approaches you considered but did not use

	I initially considered to host the file on Heroku will and create a Django app to share the file, but I was short on time so I simply decided to keep it on github as time was running out and I felt like I had to submit my work, hosting my software of pieces of it remotely allows me to modify the source code without having to update the iso to run it properly.
	Some of the software i use is only hosted on my companies share so it cannot be accessed outside the company and the usb s i run them on send me alert if anyone attempts to run the commands outside the company. 


Your personal opinions about best practices or philosophies that lead to quality code
	
	When i code i approach each line of code by thinking asking myself if there is any build in function that those what i am looking for and doing the research, if there is not i ask myself if it is worth writing one myself.

	I also like to avoid over complicating things with loops, and try to think at the hardware side of things, e.g managing memory, reducing the number of clock cycles required to achieve a task.

	I also believe in using descriptive names that will make my code understandable even without comments. I once  heard in a talk that the problem with comments is that once you update the code you must update the comments too and that is often not done well in teams, i like to have my code speak for itself as much as i can.

	Thinking ahead of problems and different ways i might want to use a function in the future is also something i do a lot.

	Finally, security is number one, before anything i ask myself how could someone use this feature against me?

If I had more time I would work on...

	The encryption system, I started thinking that my mathematical knowledge was going to be enough to finish it in a short time but ended up taking too long.

	Stripping down the operating system.

	The comments and README.txt, I would have put more accurate comments and proof read this page and the comments for spelling and grammar errors. 

